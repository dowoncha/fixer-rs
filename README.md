# fixer-rs
Rust Api wrapper for fixer.io foreign currency api

Comes with a library for integrating into your codebase and a cli app

## Build
```
cargo run --release
```

There will be a `fixer` executable in `target/release`

## Usage
You must provide an api key through --api-key option

### Commands
* Latest
* Historical
* Fluctuation
* Timeseries
* Convert

```
$ fixer --api-key KEY latest --base USD --symbols EUR,JPY
$ fixer --api-key KEY historical 2018-10-10
```

## Using the library
The fixer api can be instantiated with an api key,
the object will contain all necessary entrypoints to each endpoint.

Fluctuation and Timeseries commands require a premium fixer io subscription
```
let api = FixerApi::new(api_key);
// Base, Symbols
api.get_latest_rates(Currency::USD, None);
```

Please view the fixer.io docs for more information.

