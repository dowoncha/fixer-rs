extern crate fixer;

extern crate clap;

use clap::{App, Arg, SubCommand};
use fixer::{FixerApi, Currency};

fn main() -> Result<(), Box<std::error::Error>> {
    let matches = App::new("fixer")
        .version("0.1")
        .about("Fixer api cli")
        .author("d. cha")
        .arg(Arg::with_name("API_KEY")
             .long("api-key")
             .value_name("KEY")
             .required(true))
        .subcommand(SubCommand::with_name("latest")
                    .about("Gets the latest exchange rates")
                    .arg(Arg::with_name("base")
                         .short("b")
                         .help("Base currency defaults to EUR")))
        .subcommand(SubCommand::with_name("historical")
                    .about("Returns historical exchange rate data for all available or a specific set of currencies"))
        .subcommand(SubCommand::with_name("timeseries")
                    .about("Returns daily historical exchange rate data between two specified dates for all available or a specific set of currencies"))
        .subcommand(SubCommand::with_name("fluctuation")
                    .about("Fluctuation data b/w two specified dates fo all available or a specific set of currencies"))
        .subcommand(SubCommand::with_name("convert")
                    .about("Allows for ocnversion of any amount from one currency to another"))
        .get_matches();

    let api_key = matches.value_of("API_KEY").unwrap();

    let api = FixerApi::new(api_key);

    if let Some(matches) = matches.subcommand_matches("latest") {
        let latest_rates = api.get_latest_rates(None, None)?;
        
        println!("{:?}", latest_rates);
    } else if let Some(matches) = matches.subcommand_matches("historical") {

    } else if let Some(matches) = matches.subcommand_matches("timeseries") {

    } else if let Some(matches) = matches.subcommand_matches("fluctuation") {

    } else if let Some(matches) = matches.subcommand_matches("convert") {

    }

    Ok(())
}
