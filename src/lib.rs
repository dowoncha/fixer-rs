extern crate serde;
extern crate serde_json;

use serde::{Serialize, Deserialize};

use std::collections::{HashMap};

#[derive(Serialize, Deserialize, Debug, Hash, PartialEq, Eq)]
pub enum Currency {
    AUD,
    // CAD,
    // CHF,
    // CNY,
    EUR,
    // GBP,
    JPY,
    USD
}

impl Currency {
    pub fn as_str(&self) -> &str {
        match self {
            Currency::AUD => "AUD",
            Currency::EUR => "EUR",
            Currency::JPY => "JPY",
            Currency::USD => "USD"
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ExchangeRate {
    timestamp: i64,
    base: Currency,
    date: String,
    rates: HashMap<Currency, f32>
}

pub struct TimeSeries {
    success: bool,
    timeseries: bool,
    start_date: String,
    end_date: String,
    base: Currency,
    rates: HashMap<String, HashMap<Currency, f32>>
}

pub struct FixerApi {
    access_key: String
}

impl FixerApi {
    pub fn new(access_key: &str) -> Self {
        Self {
            access_key: access_key.to_string()
        }
    }

    pub fn get_latest_rates(&self, base: Option<Currency>, symbols: Option<&[Currency]>) -> Result<ExchangeRate, reqwest::Error> {
        let latest_rate = self.get_historical_rates("latest", base, symbols);

        latest_rate
    }

    pub fn get_historical_rates(&self, date: &str, base: Option<Currency>, symbols: Option<&[Currency]>) -> Result<ExchangeRate, reqwest::Error> {
        // Validate date format
        // YYYY-MM-DD

        // let url = format!(
        //     "http://data.fixer.io/api/{date}?access_key={access_key}&base={base}&symbols={symbols}",
        //     access_key = self.access_key,
        //     date = date,
        //     base = "EUR",
        //     symbols = "USD,JPY,AUD"
        // );
        // &[("base", base.as_str())]
        let url = self.get_url(date, &[("symbols", "AUD,EUR,JPY,USD")]);

        let mut response = reqwest::get(&url)?;
        let body = response.text()?;

        let rate: ExchangeRate = serde_json::from_str(&body).unwrap();

        Ok(rate)
    }

    pub fn convert(&self, amount: f32, from: Currency, to: Currency, date: Option<&str>) -> Result<f32, reqwest::Error> {
        Ok(amount)
    }
    
    pub fn get_timeseries(&self, start_date: &str, end_date: &str, base: Option<Currency>, symbols: Option<&[Currency]>) {
        
    }

    pub fn get_fluctuation(&self, start_date: &str, end_date: &str) -> Result<(), reqwest::Error> {
        let url = self.get_url(
            "fluctuation", 
            &[("start_date", start_date), 
            ("end_date", end_date)]);

        let mut response = reqwest::get(&url)?;
        let body = response.text()?;

        Ok(())
    }

    fn get_url(&self, endpoint: &str, params: &[(&str, &str)]) -> String {
        let mut args = String::new();

        for param in params {
            args.push('&');
            args.push_str(param.0);
            args.push('=');
            args.push_str(param.1);
        }

        let url = format!(
            "http://data.fixer.io/api/{endpoint}?access_key={access_key}{args}",
                endpoint = endpoint,
                access_key = self.access_key,
                args = args);
        url
    }
}
